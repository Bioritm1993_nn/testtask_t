import static org.junit.jupiter.api.Assertions.*;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.*;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HttpRequestManagerTest {

    private static Logger logger = Logger.getLogger(HttpRequestManager.class.getSimpleName());
    private static FileHandler fileHandler;

    @BeforeClass
    public static void initLogger() {
        if (null == fileHandler) {
            try {
                fileHandler = new FileHandler("%tLogsTestTask_Tests");
                logger.addHandler(fileHandler);
            } catch (SecurityException e) {
                logger.log(Level.SEVERE,
                        "SecurityException happened;",
                        e);
            } catch (IOException e) {
                logger.log(Level.SEVERE,
                        "IOException happened;",
                        e);
            }
        }
    }

    @AfterClass
    public static void deInitLogger() {
        if (null != fileHandler) {
            logger.removeHandler(fileHandler);
        }
        fileHandler = null;
        logger = null;
    }

    /**
     * Test request "headers". Response should be JSON with headers dict
     * @throws Exception
     */
    @Test
    public void function_headers() throws Exception {
        logger.log(Level.INFO, "test function_headers() is called.");
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject) parser.parse(HttpRequestManager.function_headers());
        JSONObject headers = (JSONObject) obj.get("headers");

        assertNotNull(headers);
    }

    /**
     * Test request "status". Response the same code. There is single-shot method!
     * @throws Exception
     */
    @Test
    public void function_status_single() throws Exception {
        logger.log(Level.INFO, "test function_status() is called.");

        int value = 200;
        assertEquals(value, HttpRequestManager.function_status(value));
    }

    /**
     * Test request "status" N-times. Response the same codes.
     */
    @Test
    @Ignore // we have some problems with 304 code; too many redirects on "BACK side"
    public void function_status() {
        logger.log(Level.INFO, "test function_status() is called.");
        int successCount = 0;
        int httpCodeMinValue = 100;
        int httpCodeMaxValue = 599;

        for (int i = httpCodeMinValue; i <= httpCodeMaxValue; i++) {
            int responseCode = 0;
            try {
                responseCode = HttpRequestManager.function_status(i);
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Cant get responseCode; failed!");
                fail("Cant get responseCode; failed!");
                e.printStackTrace();
            }

            if (i == responseCode) {
                successCount++;
            } else {
                logger.log(Level.SEVERE, "wrong httpResponseCode! Expected : " + i +
                        "   Actual : " + responseCode);
            }
        }

        int expected = httpCodeMaxValue - httpCodeMinValue;
        assertEquals(expected, successCount);
    }

    /**
     * Test request "redirect".
     * @throws Exception
     */
    @Test
    public void function_redirect() throws Exception {
        logger.log(Level.INFO, "test function_redirect() is called.");
        int successCount = 0;
        int maxRedirects = 19; // 20 --> http exception

        for (int i = 1; i <= maxRedirects; i++) {
            HttpRequestManager.function_redirect(i);
            if (i == HttpRequestManager.redirectsCount) {
                successCount++;
            } else {
                logger.log(Level.SEVERE, "wrong redirectsCount! Expected : " + i +
                        "   Actual : " + HttpRequestManager.redirectsCount);
            }
        }

        assertEquals(maxRedirects, successCount);
    }
}