import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.*;

public class HttpRequestManager {
    private final static String USER_AGENT = "Mozilla/5.0";
    private final static String HTTP_GET = "GET";
    private final static String BASE_URL = "http://httpbin.org";
    public static int redirectsCount = 0;

    private static Logger logger = Logger.getLogger(HttpRequestManager.class.getSimpleName());
    public static FileHandler fileHandler;

    public static void initLogger() {
        if(null == fileHandler) {
            try {
                fileHandler = new FileHandler("%tLogsTestTask");
                logger.addHandler(fileHandler);
            } catch (Exception e) {
                // ignored
            }
        }
    }

    /**
     * 1st function from task. Request headers.
     * @return content with headers
     * @throws Exception
     */
    public static String function_headers() throws Exception {
        initLogger();

        String url = BASE_URL + '/' + "headers";

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod(HTTP_GET);
        con.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = con.getResponseCode();

        logger.log(Level.INFO, "\nSending 'GET' request to URL : " + url);
        logger.log(Level.INFO, "Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        String inputLine;
        StringBuffer response = new StringBuffer();

        while (null != (inputLine = in.readLine())) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }

    /**
     * 2nd function from task. Request httpCode.
     * @param code -- http code
     * @return the same http code
     * @throws Exception
     */
    public static int function_status(int code) throws Exception {
        initLogger();

        String url = BASE_URL + '/' + "status" + '/' + String.valueOf(code);

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod(HTTP_GET);
        con.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = con.getResponseCode();

        logger.log(Level.INFO, "\nSending 'GET' request to URL : " + url);
        logger.log(Level.INFO, "Response Code : " + responseCode);

        return responseCode;
    }

    /**
     * Redirects N times in row
     * @param redirectCount
     * @return
     * @throws Exception
     */
    public static String function_redirect(int redirectCount) throws Exception {
        initLogger();

        String url = BASE_URL + "/redirect/" + String.valueOf(redirectCount);
        redirectsCount = 0;

        URL obj = new URL(url);

        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // для того, чтоб редиректы на этом сайте обрабатывались
        con.setInstanceFollowRedirects(false);
        con.setRequestMethod(HTTP_GET);
        con.setRequestProperty("User-Agent", USER_AGENT);


        boolean isRedirect = false;
        int responseCode = con.getResponseCode();

        logger.log(Level.INFO, "\nSending 'GET' request to URL : " + url);
        logger.log(Level.INFO, "Response Code : " + responseCode);

        if (responseCode != HttpURLConnection.HTTP_OK) {
            if (responseCode == HttpURLConnection.HTTP_MOVED_TEMP ||
                    responseCode == HttpURLConnection.HTTP_MOVED_PERM) {
                isRedirect = true;
            }
        }

        while (isRedirect) {
            redirectsCount++;

            String newUrl = BASE_URL + con.getHeaderField("Location");

            con = (HttpURLConnection) new URL(newUrl).openConnection();

            con.setInstanceFollowRedirects(false);
            con.setRequestMethod(HTTP_GET);
            con.setRequestProperty("User-Agent", USER_AGENT);

            responseCode = con.getResponseCode();

            logger.log(Level.INFO, "\nSending 'GET' request to URL : " + newUrl);
            logger.log(Level.INFO, "Response Code : " + responseCode);

            if (responseCode != HttpURLConnection.HTTP_MOVED_TEMP &&
                    responseCode != HttpURLConnection.HTTP_MOVED_PERM) {
                isRedirect = false;
            }
        }

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        String inputLine;
        StringBuffer response = new StringBuffer();

        while (null != (inputLine = in.readLine())) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }
}